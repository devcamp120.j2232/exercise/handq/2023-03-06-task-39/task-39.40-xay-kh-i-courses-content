"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gCoursesDB = {
    description: "This DB includes all courses in system",
    courses: [
        {
            id: 1,
            courseCode: "FE_WEB_ANGULAR_101",
            courseName: "How to easily create a website with Angular",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-angular.jpg",
            teacherName: "Morris Mccoy",
            teacherPhoto: "images/teacher/morris_mccoy.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 2,
            courseCode: "BE_WEB_PYTHON_301",
            courseName: "The Python Course: build web application",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-python.jpg",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        },
        {
            id: 5,
            courseCode: "FE_WEB_GRAPHQL_104",
            courseName: "GraphQL: introduction to graphQL for beginners",
            price: 850,
            discountPrice: 650,
            duration: "2h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-graphql.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 6,
            courseCode: "FE_WEB_JS_210",
            courseName: "Getting Started with JavaScript",
            price: 550,
            discountPrice: 300,
            duration: "3h 34m",
            level: "Beginner",
            coverImage: "images/courses/course-javascript.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 8,
            courseCode: "FE_WEB_CSS_111",
            courseName: "CSS: ultimate CSS course from beginner to advanced",
            price: 750,
            discountPrice: 600,
            duration: "3h 56m",
            level: "Beginner",
            coverImage: "images/courses/course-css.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 9,
            courseCode: "FE_WEB_WORDPRESS_111",
            courseName: "Complete Wordpress themes & plugins",
            price: 1050,
            discountPrice: 900,
            duration: "4h 30m",
            level: "Intermediate",
            coverImage: "images/courses/course-wordpress.jpg",
            teacherName: "Clevaio Simon",
            teacherPhoto: "images/teacher/clevaio_simon.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 10,
            courseCode: "FE_UIUX_COURSE_211",
            courseName: "Thinkful UX/UI Design Bootcamp",
            price: 950,
            discountPrice: 700,
            duration: "5h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-uiux.jpg",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: false,
            isTrending: false
        },
        {
            id: 11,
            courseCode: "FE_WEB_REACRJS_210",
            courseName: "Front-End Web Development with ReactJs",
            price: 1100,
            discountPrice: 850,
            duration: "6h 20m",
            level: "Advanced",
            coverImage: "images/courses/course-reactjs.jpg",
            teacherName: "Ted Hawkins",
            teacherPhoto: "images/teacher/ted_hawkins.jpg",
            isPopular: true,
            isTrending: true
        },
        {
            id: 12,
            courseCode: "FE_WEB_BOOTSTRAP_101",
            courseName: "Bootstrap 4 Crash Course | Website Build & Deploy",
            price: 750,
            discountPrice: 600,
            duration: "3h 15m",
            level: "Intermediate",
            coverImage: "images/courses/course-bootstrap.png",
            teacherName: "Juanita Bell",
            teacherPhoto: "images/teacher/juanita_bell.jpg",
            isPopular: true,
            isTrending: false
        },
        {
            id: 14,
            courseCode: "FE_WEB_RUBYONRAILS_310",
            courseName: "The Complete Ruby on Rails Developer Course",
            price: 2050,
            discountPrice: 1450,
            duration: "8h 30m",
            level: "Advanced",
            coverImage: "images/courses/course-rubyonrails.png",
            teacherName: "Claire Robertson",
            teacherPhoto: "images/teacher/claire_robertson.jpg",
            isPopular: false,
            isTrending: true
        }
    ]
}

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// gán sự kiện load trang/F5
$(document).ready(onPageLoading);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function xử lý sựu kiện load trang/F5
function onPageLoading() {
    loadRecommendedCourses();
    loadMostPopularCourses();
    loadTrendingCourse();
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// function tạo card thông tin khóa học
function createCourseCard(paramCard, paramCourseData) {
    return paramCard.html(
        `
            <img class="card-img-top" src=${paramCourseData.coverImage}>
            <div class="card-body">
                <p class="text-primary"><b>${paramCourseData.courseName}</b></p>
                <p><i class="fa-regular fa-clock"></i> ${paramCourseData.duration} &nbsp; ${paramCourseData.level}</p>
                <p><span><b>$${paramCourseData.discountPrice}</b></span> <s>$${paramCourseData.price}</s></p>
            </div>
            <div class="card-footer" style="height: 50px">
                <img src=${paramCourseData.teacherPhoto} class="rounded-circle d-inline-block" style="width: 30px">
                <p class="d-inline-block ml-3"><small>${paramCourseData.teacherName}</small></p>
                <i role="button" class="fa-regular fa-bookmark my-auto float-right"></i>
            </div>
        `
    );
}

// function load recommended course
function loadRecommendedCourses() {
    let vCourses = gCoursesDB.courses;
    for (let bIndex = 0; bIndex < 4; bIndex ++) {
        let bNewCard = $("<card>").addClass("card card-hover").data("id", vCourses[bIndex].id).appendTo("#card-deck-recommended");
        createCourseCard(bNewCard, vCourses[bIndex]);
    }
}

// function load recommended course
function loadMostPopularCourses() {
    // dùng filter lấy danh sách khóa học most popular
    let vPopularCourses = gCoursesDB.courses.filter(function(course) {
        return course.isPopular == true;
    });
    // tạo card các khóa học popular
    for (let bIndex = 0; bIndex < 4; bIndex ++) {
        let bNewCard = $("<card>").addClass("card card-hover").data("id", vPopularCourses[bIndex].id).appendTo("#card-deck-popular");
        createCourseCard(bNewCard, vPopularCourses[bIndex]);
    }
}

// function load trending course
function loadTrendingCourse() {
    // dùng filter lấy danh sách khóa học most popular
    let vTrendingCourse = gCoursesDB.courses.filter(function(course) {
        return course.isTrending == true;
    });
    // tạo card các khóa học trending
    for (let bIndex = 0; bIndex < 4; bIndex ++) {
        let bNewCard = $("<card>").addClass("card card-hover").data("id", vTrendingCourse[bIndex].id).appendTo("#card-deck-trending");
        createCourseCard(bNewCard, vTrendingCourse[bIndex]);
    }
}